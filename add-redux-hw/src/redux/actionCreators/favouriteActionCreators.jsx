import { SET_FAVOURITE } from "../actions/goodsActions";

export const setFavouriteCard = (id) => ({type: SET_FAVOURITE, payload: id})
