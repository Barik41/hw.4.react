import React from "react";
import FavouriteContainer from "../../components/FavouriteContainer/FavouriteContainer";

const Favourite = () => {

    return(
        <div className="container">
            <FavouriteContainer />
        </div>
    )
}

export default Favourite;